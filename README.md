# README till game.js

### I nedanstående README fil finns instruktioner för hur räknespelet fungerar. 

Räknespelet jag skapat går ut på att svara rätt på ett antal frågor inom matematik. Dessa frågor innehåller två slumpartade variabler där användaren väljer maximivärdet på variablerna. 

För att starta räknespelet krävs det att följande linje skrivs in: node game.js

### User (Användare)

Det första användaren måste göra för att starta spelet, samt spara sina resultat är att välja ett användarnamn. Detta görs genom att använda flaggan --user, därefter skriver användaren sitt namn. Exempelvis

- --user Lukas (vilket då ger användarnamnet Lukas.)

Resultaten sparas sedan i en fil. Dessa resultat hittas under ./Data/*InsertUsername*. För försök nr1 kommer filen heta *InsertUsername*1, försök nr2 *InsertUsername*2, etc.

### Operation (Räknesätt)

Användaren väljer själv räknesätt genom att använda flaggan --o (för operation). Bokstäverna i listan ovan visar symbolen för räknesättet. Exempelvis ger --o m multiplikation ut, då symbolen *m* motsvarar just multiplikation. Tabellen nedan visar symbolerna för de fyra räknesätten:

- addition (a)
- subtraktion (s) 
- multiplikation (m)
- division (d)

En fråga skulle exempelvis kunna se ut på följande sätt: 9x15=?
Rätt svar i just detta fallet skulle då vara 135. 

### Difficulty (Maxvärde)

Användaren kan också välje maxvärdet på de variabler som skall användas i beräkningarna. Detta görs genom flaggan --d (difficulty) vartefter användaren skriver maximivärdet. Exempelvis

- --d 100 (vilket då ger ett maximivärde på 100)

Med --d 100 skulle en fråga då kunna se ut på följande sätt: 74 + 33=?
Rätt svar i just detta fallet skulle då vara 107.

### Questions (Frågor)

Slutligen kan användaren välja hur många frågor denne vill svara på. Detta görs genom flaggan --q (questions) vartefter användaren skriver antalet frågor hen vill motta. Exempelvis

- --q 30 (ger då 30 frågor)

### En full fråga kan exempelvis se ut på följande sätt:

node game.js --user Lukas --o a --d 10 --q 10

Denna linje kod ger då:

- Användarnamnet Lukas
- Räknesättet Addition
- Maximivärdet 10
- Frågemängden 10

Detta är allt du behöver veta om mitt räknespel.


